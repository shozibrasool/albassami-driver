package com.albassami.driver.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.albassami.driver.utils.newutils.sharedpref.PrefKeys;
import com.albassami.driver.utils.newutils.sharedpref.PrefUtils;

public class InstanceIdServices extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        saveDeviceToken(refreshedToken);
    }

    private void saveDeviceToken(String token) {
        PrefUtils.getInstance(this).setValue(PrefKeys.FCM_TOKEN, token);
    }
}
