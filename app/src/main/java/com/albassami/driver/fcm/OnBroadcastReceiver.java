package com.albassami.driver.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.albassami.driver.ui.activity.NoInternetActivity;

import static com.albassami.driver.ui.activity.NoInternetActivity.isInternetActivityCalled;

/**
 * Created by user on 1/2/2017.
 */

public class OnBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
        } else {
            if(!isInternetActivityCalled) {
                Intent noInternetActivity = new Intent(context, NoInternetActivity.class);
                noInternetActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(noInternetActivity);
            }
        }
    }

}