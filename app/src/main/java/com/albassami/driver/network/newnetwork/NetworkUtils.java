package com.albassami.driver.network.newnetwork;

import android.content.Context;
import android.net.ConnectivityManager;

import com.albassami.driver.R;
import com.albassami.driver.utils.newutils.UiUtils;

public class NetworkUtils {

    public static void onApiError(Context context) {
        if (context == null) return;
        UiUtils.hideLoadingDialog();
        UiUtils.showShortToast(context, context.getString(R.string.something_went_wrong));
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = null;
        try {
            cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert cm != null;
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
