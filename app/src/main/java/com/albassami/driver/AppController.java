package com.albassami.driver;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.albassami.driver.utils.ForegroundListner;
import com.albassami.driver.utils.newutils.sharedpref.PrefKeys;
import com.albassami.driver.utils.newutils.sharedpref.PrefUtils;

import io.fabric.sdk.android.Fabric;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class AppController extends Application implements Application.ActivityLifecycleCallbacks {


    public static final String TAG = AppController.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static AppController mInstance;
    PrefUtils prefUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        prefUtils = PrefUtils.getInstance(getApplicationContext());
        mInstance = this;
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        ForegroundListner.init(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }


    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, true);
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, true);
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, true);
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, false);
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, false);
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        prefUtils.setValue(PrefKeys.IS_ACTIVITY_FOREGROUND, false);
    }
}

