package com.albassami.driver.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.albassami.driver.R;
import com.albassami.driver.utils.splash.SplashAnimationHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahesh on 7/19/2017.
 */

public class SplashActivity extends AppCompatActivity {
    @BindView(R.id.tv_app_name)
    ImageView tvAppName;
    @BindView(R.id.splashAnimationLayout)
    RelativeLayout splashAnimationLayout;
    private SplashAnimationHelper.SplashRouteAnimation splashRouteAnimation;
    int versionCode;
    int mDriver_Version_Code;
    AppUpdateManager appUpdateManager;
    InstallStateUpdatedListener listener;
    private static final int RC_APP_UPDATE = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animateToHomeScreen();
        startProgressAnimation();
        new Handler().postDelayed(() -> {
//            Intent i = new Intent(SplashActivity.this, GetStartedActivity.class);
            Intent i = new Intent(SplashActivity.this, GetStartedActivity.class);
            startActivity(i);
            finish();
        }, 1000);
        setContentView(R.layout.layout_animation);
        ButterKnife.bind(this);
        checkForAnAppUpdate();
    }

    public void checkForAnAppUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateManager.registerListener(listener);
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo, AppUpdateType.FLEXIBLE, this, RC_APP_UPDATE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        listener = installState -> {
            try {
                if (installState.installStatus() == InstallStatus.DOWNLOADED) {
                    popupSnackbarForCompleteUpdate();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        };
    }

    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar =
                Snackbar.make(
                        findViewById(R.id.splashAnimationLayout),
                        getString(R.string.updateAvailable),
                        Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.restartCaps), view -> appUpdateManager.completeUpdate());
        snackbar.setActionTextColor(
                getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private void startProgressAnimation() {
        this.splashRouteAnimation = new SplashAnimationHelper().createSplashAnimation(this);
        //this.splashRouteAnimation.startAnimation(this.splashAnimationLayout);
    }

    private void animateToHomeScreen() {
        AnimatorSet localAnimatorSet1 = new AnimatorSet();
        AnimatorSet localAnimatorSet2 = new AnimatorSet();
        AnimatorSet localAnimatorSet3 = new AnimatorSet();
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        localArrayList1.add(ObjectAnimator.ofFloat(this.splashAnimationLayout, "alpha", new float[]{1.0F, 0.0F}));
        localAnimatorSet2.setDuration(1000);
        localAnimatorSet2.playTogether(localArrayList1);
        localAnimatorSet3.playSequentially(localArrayList2);
        localAnimatorSet3.setDuration(500L);
        localAnimatorSet3.setStartDelay(50L);
        localAnimatorSet1.playSequentially(new Animator[]{localAnimatorSet2, localAnimatorSet3});
        localAnimatorSet1.addListener(new Animator.AnimatorListener() {
            public void onAnimationCancel(Animator paramAnonymousAnimator) {
            }

            public void onAnimationEnd(Animator paramAnonymousAnimator) {
                if (SplashActivity.this.splashRouteAnimation != null) {

                }
            }

            public void onAnimationRepeat(Animator paramAnonymousAnimator) {
            }

            public void onAnimationStart(Animator paramAnonymousAnimator) {
            }
        });
        localAnimatorSet1.start();
    }
}
