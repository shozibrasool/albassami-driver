package com.albassami.driver.ui.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.albassami.driver.network.APIManager.AsyncTaskCompleteListener;
import com.albassami.driver.ui.activity.MainActivity;

/**
 * Created by user on 1/5/2017.
 */

public class BaseMapFragment extends Fragment implements
        View.OnClickListener, AsyncTaskCompleteListener {
    MainActivity activity;
    public static LatLng pic_latlan;
    public static LatLng drop_latlan;
    public static boolean searching =false;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();


    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

    }

    @Override
    public void onClick(View v) {


    }
}
