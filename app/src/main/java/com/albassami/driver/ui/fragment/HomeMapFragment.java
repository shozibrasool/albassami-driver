package com.albassami.driver.ui.fragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.albassami.driver.R;
import com.albassami.driver.network.location.LocationHelper;
import com.albassami.driver.network.model.AdsList;
import com.albassami.driver.network.model.TaxiTypes;
import com.albassami.driver.network.newnetwork.APIClient;
import com.albassami.driver.network.newnetwork.APIConstants;
import com.albassami.driver.network.newnetwork.APIInterface;
import com.albassami.driver.network.newnetwork.NetworkUtils;
import com.albassami.driver.ui.adapter.AdsAdapter;
import com.albassami.driver.ui.adapter.TaxiAdapter;
import com.albassami.driver.utils.Commonutils;
import com.albassami.driver.utils.Const;
import com.albassami.driver.utils.ItemClickSupport;
import com.albassami.driver.utils.ParseContent;
import com.albassami.driver.utils.newutils.UiUtils;
import com.albassami.driver.utils.newutils.sharedpref.PrefKeys;
import com.albassami.driver.utils.newutils.sharedpref.PrefUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahesh on 1/5/2017.
 */

public class HomeMapFragment extends BaseMapFragment implements LocationHelper.OnLocationReceived, GoogleMap.OnCameraMoveListener, OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private static final int DURATION = 2000;
    @BindView(R.id.recycAds)
    RecyclerView recycAds;
    @BindView(R.id.btn_mylocation)
    ImageButton myLoction;
    @BindView(R.id.design_bottom_sheet)
    RelativeLayout designBottomSheet;
    @BindView(R.id.imageViewArrow)
    ImageView imageViewArrow;
    private GoogleMap googleMap;
    private Bundle mBundle;
    private static SupportMapFragment req_home_map;
    private View view;
    private LocationHelper locHelper;
    private Location myLocation;
    private LatLng latlong;
    private ArrayList<TaxiTypes> typesList = new ArrayList<>();
    private TaxiAdapter taxiAdapter;
    private AdsAdapter adsAdapter;
    private List<AdsList> adsLists;
    private String TAG = HomeMapFragment.class.getSimpleName();
    private BottomSheetBehavior behavior;
    APIInterface apiInterface;
    PrefUtils prefUtils;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_map_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        req_home_map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.req_home_map);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        prefUtils = PrefUtils.getInstance(activity);
        if (null != req_home_map) {
            req_home_map.getMapAsync(this);
        }
        setUpBottomSheet();
        setUpAdapter();
        manageAds();
        return view;
    }

    private void setUpBottomSheet() {
        final View bottomSheet = view.findViewById(R.id.design_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                rotateArrow(slideOffset);
            }
        });
        behavior.setHideable(false);
        behavior.setSkipCollapsed(false);
        imageViewArrow.setOnClickListener(v -> {
            if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private void setUpAdapter() {
        recycAds.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycAds.setItemAnimator(new DefaultItemAnimator());
        ItemClickSupport.addTo(recycAds).setOnItemClickListener((recyclerView, position, v) -> {
            try{
                String url = adsLists.get(position).getAdUrl();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }


    public void manageAds() {
        Call<String> call = apiInterface.manageAds(prefUtils.getIntValue(PrefKeys.ID, 0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject job1 = new JSONObject(response.body());
                    if (job1.optString(APIConstants.Params.SUCCESS).equals(APIConstants.Constants.TRUE)) {
                        JSONArray jsonArray = job1.optJSONArray(APIConstants.Params.DATA);
                        if (null != adsLists) {
                            adsLists.clear();
                        }
                        if (null != jsonArray && jsonArray.length() > 0) {
                            adsLists = new ParseContent(activity).parseAdsList(jsonArray);
                            if (adsLists != null) {
                                adsAdapter = new AdsAdapter(adsLists, activity);
                                recycAds.setAdapter(adsAdapter);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if(NetworkUtils.isNetworkConnected(getActivity())) {
                    UiUtils.showShortToast(getActivity(), getString(R.string.may_be_your_is_lost));
                }
            }
        });

    }

    private void rotateArrow(float v) {
        imageViewArrow.setRotation(-180 * v);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            MapsInitializer.initialize(activity);
        } catch (Exception e) {
        }
        locHelper = new LocationHelper(activity);
        locHelper.setLocationReceivedLister(this);
    }

    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            myLocation = location;
            LatLng latLang = new LatLng(location.getLatitude(),
                    location.getLongitude());
            latlong = latLang;
        }
    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {

        if (location != null && googleMap != null) {
            LatLng currentlatLang = new LatLng(location.getLatitude(), location.getLongitude());
            Log.d("Current Location", "onConntected: " + currentlatLang);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentlatLang, 16));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.HOME_MAP_FRAGMENT;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SupportMapFragment f = (SupportMapFragment) getFragmentManager()
                .findFragmentById(R.id.req_home_map);
        if (f != null) {
            try {
                getFragmentManager().beginTransaction().remove(f).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ViewGroup mContainer = getActivity().findViewById(R.id.content_frame);
        mContainer.removeAllViews();
        googleMap = null;
    }

    @Override
    public void onMapReady(GoogleMap mgoogleMap) {
        googleMap = mgoogleMap;
        if (googleMap != null) {
            Commonutils.progressdialog_hide();
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setMapToolbarEnabled(true);
            googleMap.getUiSettings().setScrollGesturesEnabled(true);
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnCameraMoveListener(this);
            googleMap.setOnCameraIdleListener(this);
        }
    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraIdle() {

    }

    @OnClick({R.id.btn_mylocation, R.id.design_bottom_sheet})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_mylocation:
                if (null != latlong)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 16));
                break;
            case R.id.design_bottom_sheet:
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                break;
        }
    }
}
