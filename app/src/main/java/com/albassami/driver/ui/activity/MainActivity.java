package com.albassami.driver.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.albassami.driver.R;
import com.albassami.driver.network.location.LocationHelper;
import com.albassami.driver.network.model.RequestDetails;
import com.albassami.driver.network.newnetwork.APIClient;
import com.albassami.driver.network.newnetwork.APIConstants;
import com.albassami.driver.network.newnetwork.APIConstants.Constants;
import com.albassami.driver.network.newnetwork.APIConstants.Params;
import com.albassami.driver.network.newnetwork.APIEvent;
import com.albassami.driver.network.newnetwork.APIInterface;
import com.albassami.driver.network.newnetwork.NetworkUtils;
import com.albassami.driver.network.newnetwork.ParserUtils;
import com.albassami.driver.ui.fragment.FeedBackFragment;
import com.albassami.driver.ui.fragment.HomeMapFragment;
import com.albassami.driver.ui.fragment.OngoingFragment;
import com.albassami.driver.utils.Const;
import com.albassami.driver.utils.PreferenceHelper;
import com.albassami.driver.utils.newutils.UiUtils;
import com.albassami.driver.utils.newutils.sharedpref.PrefHelper;
import com.albassami.driver.utils.newutils.sharedpref.PrefKeys;
import com.albassami.driver.utils.newutils.sharedpref.PrefUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.albassami.driver.ui.fragment.NavigationDrawableFragment.ivUserIcon;

public class MainActivity extends AppCompatActivity implements LocationHelper.OnLocationReceived, EasyPermissions.PermissionCallbacks{

    private final long interval = 1 * 1000;
    public String currentFragment = "";
    @BindView(R.id.bnt_menu)
    ImageButton bntMenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    MediaPlayer mediaPlayer;
    private ActionBarDrawerToggle drawerToggle;
    private Handler reqhandler;
    private Handler requestStatusHandler;
    private AlertDialog gpsAlertDialog, internetDialog;
    AlertDialog.Builder gpsBuilder;
    private MyCountDownTimer countDownTimer;
    RequestDetails requestDetails;
    String request_id;
    APIInterface apiInterface;
    PrefUtils prefUtils;
    private int RC_LOCATION_PERM = 124;

    Runnable runnable = new Runnable() {
        public void run() {
            getIncomingRequestsInProgress();
            reqhandler.postDelayed(this, 5000);
        }
    };

    Runnable requestStatusRunnable = new Runnable() {
        public void run() {
            requestStatusCheck();
            requestStatusHandler.postDelayed(this, 5000);
        }
    };

    private Dialog requestDialog;
    TextView tv_timer;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra(Const.PROVIDER_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                JSONObject jsonObject = messageObj.optJSONObject(Params.DATA);

                if (Integer.parseInt(jsonObject.getString("status")) == 6) {
                    if (requestDialog != null && requestDialog.isShowing()) {
                        requestDialog.cancel();
                        mediaPlayer.stop();
                        startCheckingUpcomingRequests();
                        countDownTimer.cancel();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setUpLocale();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        prefUtils = PrefUtils.getInstance(this);
        getPermission();
        initDrawer();
        reqhandler = new Handler();
        requestStatusHandler = new Handler();
    }

    private void getPermission() {
        EasyPermissions.requestPermissions(this, getString(R.string.needs_location_permission),
                RC_LOCATION_PERM, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        registerEventBus();
    }

    private void setUpLocale() {
        if (!TextUtils.isEmpty(new PreferenceHelper(this).getLanguage())) {
            Locale myLocale = null;
            switch (new PreferenceHelper(this).getLanguage()) {
                case "":
                    myLocale = new Locale("en");
                    break;
                case "en":
                    myLocale = new Locale("en");

                    break;
                case "fr":
                    myLocale = new Locale("fr");
                    break;
            }
            Locale.setDefault(myLocale);
            Configuration config = new Configuration();
            config.locale = myLocale;
            this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTokenExpiry(APIEvent event) {
        unregisterEventBus();
        logOutUserFromDevice();
    }

    private void logOutUserFromDevice() {
        PrefHelper.setUserLoggedOut(MainActivity.this);
        prefUtils.setValue(PrefKeys.IS_LOGGED_IN, false);
        Intent restartActivity = new Intent(MainActivity.this, SplashActivity.class);
        restartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(restartActivity);
        MainActivity.this.finish();
    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Glide.with(getApplicationContext())
                        .load(prefUtils.getStringValue(PrefKeys.PICTURE, ""))
                        .thumbnail(.4f)
                        .placeholder(R.drawable.account)
                        .into(ivUserIcon);
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.post(() -> drawerToggle.syncState());
    }

    public void startCheckRegTimer() {
        reqhandler.postDelayed(runnable, 5000);
    }

    public void startCheckingRequest() {
        requestStatusHandler.postDelayed(requestStatusRunnable, 5000);
    }

    public void getIncomingRequestsInProgress() {
        Call<String> call = apiInterface.incomingRequest(prefUtils.getIntValue(PrefKeys.ID, 0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String number_hours = "";
                JSONObject incomingResponse;
                try {
                    incomingResponse = new JSONObject(response.body());
                    if (incomingResponse != null) {
                        if (incomingResponse.optString(Params.SUCCESS).equals(Constants.TRUE)) {
                            JSONArray incomingReqArray = incomingResponse.getJSONArray(Params.DATA);
                            if(incomingReqArray.length() != 0) {
                                Log.e("Incoming request", String.valueOf(incomingReqArray));
                                JSONObject requestObject = incomingReqArray.getJSONObject(0);
                                String picture = requestObject.optString(Params.USER_PICTURE);
                                String name = requestObject.optString(Params.USER_NAME);
                                request_id = requestObject.optString(Params.REQUEST_ID);
                                String address = requestObject.optString(Params.S_ADDRESS);
                                String s_lat = requestObject.optString(Params.S_LATITUDE);
                                String s_lan = requestObject.optString(Params.S_LONGITUDE);
                                String request_status_type = requestObject.optString(Params.REQUEST_STATUS_TYPE);
                                if (request_status_type.equals("2")) {
                                    JSONObject hourObj = incomingResponse.getJSONObject(Params.HOURLY_PACKAGE_DEATILS);
                                    number_hours = hourObj.getString(Params.NUMBER_HOURS);
                                }
                                String staticMapUrl = "http://maps.google.com/maps/api/staticmap?center=" + s_lat + "," + s_lan + "&markers=" + s_lat + "," + s_lan + "&zoom=14&size=270x270&sensor=false&key=" + Const.GOOGLE_API_KEY;
                                long countDown = Long.parseLong(requestObject.optString("time_left_to_respond"));
                                String request_type = requestObject.getString(Params.REQUEST_TYPE);
                                Log.e("RequestType ", request_type);
                                if (countDown > 0) {
                                    showRequestDialog(picture, name, request_id, address, countDown, staticMapUrl, request_status_type, number_hours);
//                                    stopCheckingUpcomingRequests();
                                }
                            }
                            else
                            {
                                if(requestDialog != null && requestDialog.isShowing())
                                    requestDialog.cancel();

                                if (mediaPlayer != null)
                                    mediaPlayer.stop();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void startCheckingUpcomingRequests() {
        startCheckRegTimer();
    }

    private void startCheckingRequestStatus() {
        startCheckRegTimer();
    }

    private void stopCheckingUpcomingRequests() {
        if (reqhandler != null) {
            reqhandler.removeCallbacks(runnable);
        }
    }

    private void stopCheckingRequest() {
        if (requestStatusHandler != null) {
            requestStatusHandler.removeCallbacks(requestStatusRunnable);
        }
    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        openExitDialog();
    }

    public void addFragment(Fragment fragment, boolean addToBackStack,
                            String tag, boolean isAnimate) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (isAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.content_frame, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void requestStatusCheck() {
        try {
            Call<String> call = apiInterface.checkRequestStatus(prefUtils.getIntValue(PrefKeys.ID, 0)
                    , prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    JSONObject statusCheckResponse = null;
                    try {
                        statusCheckResponse = new JSONObject(response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (statusCheckResponse != null) {
                        if (!statusCheckResponse.optString(Params.SUCCESS).equalsIgnoreCase(Constants.TRUE)) {
                            if (statusCheckResponse.optInt(Params.ERROR_CODE) == Constants.INVALID_TOKEN) {
                                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(i);
                            } else if (statusCheckResponse.optInt(Params.ERROR_CODE) == Constants.REQUEST_ID_NOT_FOUND) {
                                UiUtils.showShortToast(MainActivity.this, "Request is not found");
                            } else if (statusCheckResponse.optInt(Params.ERROR_CODE) == Constants.INVALID_REQUEST_ID) {
                                startCheckingUpcomingRequests();
                            }
                            return;
                        } else {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.body());
                                if (jsonObject.optString(Params.SUCCESS).equals(Constants.TRUE)) {
                                    String dataObject = String.valueOf(jsonObject.optInt(Params.REQUEST_CANCELLED));
                                    requestDetails = ParserUtils.parseRequestStatus(response.body());
                                    if (requestDetails != null) {
                                        Bundle bundle = new Bundle();
                                        if (mediaPlayer != null && mediaPlayer.isPlaying())
                                            mediaPlayer.stop();
                                        OngoingFragment travelMapFragment = new OngoingFragment();
                                        switch (requestDetails.getProviderStatus()) {
                                            case APIConstants.NO_REQUEST:
                                                addFragment(new HomeMapFragment(), false, APIConstants.HOME_FRAGMENT, true);
                                                startCheckRegTimer();
                                                break;
                                            case Constants.IS_PROVIDER_ACCEPTED:
                                                bundle.putSerializable(Constants.REQUEST_DETAIL, requestDetails);
                                                bundle.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_ACCEPTED);
                                                travelMapFragment.setArguments(bundle);
                                                addFragment(travelMapFragment, false, Constants.TRAVEL_MAP_FRAGMENT, true);
                                                stopCheckingUpcomingRequests();
                                                break;
                                            case Constants.IS_PROVIDER_STARTED:
                                                bundle.putSerializable(Constants.REQUEST_DETAIL, requestDetails);
                                                bundle.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_STARTED);
                                                travelMapFragment.setArguments(bundle);
                                                addFragment(travelMapFragment, false, Constants.TRAVEL_MAP_FRAGMENT, true);
                                                stopCheckingUpcomingRequests();
                                                break;
                                            case Constants.IS_PROVIDER_ARRIVED:
                                                bundle.putSerializable(Constants.REQUEST_DETAIL, requestDetails);
                                                bundle.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_ARRIVED);
                                                travelMapFragment.setArguments(bundle);
                                                addFragment(travelMapFragment, false, Constants.TRAVEL_MAP_FRAGMENT, true);
                                                stopCheckingUpcomingRequests();
                                                break;
                                            case Constants.IS_PROVIDER_SERVICE_STARTED:
                                                bundle.putSerializable(Constants.REQUEST_DETAIL, requestDetails);
                                                bundle.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_SERVICE_STARTED);
                                                travelMapFragment.setArguments(bundle);
                                                addFragment(travelMapFragment, false, Constants.TRAVEL_MAP_FRAGMENT, true);
                                                stopCheckingUpcomingRequests();
                                                break;
                                            case Constants.IS_PROVIDER_SERVICE_COMPLETED:
                                                bundle.putSerializable(Constants.REQUEST_DETAIL, requestDetails);
                                                bundle.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_SERVICE_COMPLETED);
                                                bundle.putString("SCHEDULE", "0");
                                                FeedBackFragment feedbackFragment = new FeedBackFragment();
                                                feedbackFragment.setArguments(bundle);
                                                feedbackFragment.setRequestId(requestDetails.getRequestId());
                                                addFragment(feedbackFragment, false, Constants.FEEDBACK_FRAGMENT, true);
                                                stopCheckingUpcomingRequests();
                                                break;

                                        }
                                    }
                                } else {

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestAccepted() {
        UiUtils.showLoadingDialog(MainActivity.this);
        Call<String> call = apiInterface.providerAccepted(prefUtils.getIntValue(PrefKeys.ID, 0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                request_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                JSONObject acceptResponse;
                try {
                    UiUtils.hideLoadingDialog();
                    acceptResponse = new JSONObject(response.body());
                    if (acceptResponse != null) {
                        if (acceptResponse.optString(Params.SUCCESS).equals(Constants.TRUE)) {
                            JSONObject data = acceptResponse.optJSONObject(Params.DATA);
                            requestDetails = ParserUtils.parseAcceptedRequest(response.body());
                            stopCheckingUpcomingRequests();
                            UiUtils.hideLoadingDialog();
                            Bundle bundle2 = new Bundle();
                            OngoingFragment tripFragment = new OngoingFragment();
                            bundle2.putInt(Constants.PROVIDER_STATUS, Constants.IS_PROVIDER_ACCEPTED);
                            bundle2.putSerializable(Const.REQUEST_DETAIL, requestDetails);
                            tripFragment.setArguments(bundle2);
                            addFragment(tripFragment, false, Constants.TRAVEL_MAP_FRAGMENT, true);
                        }
                    } else {
                        UiUtils.hideLoadingDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (NetworkUtils.isNetworkConnected(getApplicationContext())) {
                    UiUtils.showShortToast(getApplicationContext(), getString(R.string.may_be_your_is_lost));
                }
            }
        });
    }

    public void driverRejected() {
        UiUtils.showLoadingDialog(MainActivity.this);
        Call<String> call = apiInterface.providerRejected(prefUtils.getIntValue(PrefKeys.ID, 0),
                prefUtils.getStringValue(PrefKeys.SESSION_TOKEN, ""),
                request_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    UiUtils.hideLoadingDialog();
                    JSONObject job1 = new JSONObject(response.toString());
                    UiUtils.hideLoadingDialog();
                    startCheckingUpcomingRequests();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (NetworkUtils.isNetworkConnected(MainActivity.this)) {
                    UiUtils.showShortToast(getApplicationContext(), getString(R.string.may_be_your_is_lost));
                }
            }
        });
    }


    private void showRequestDialog(String picture, String name, final String request_id, String address, long countDown, String map_img, String req_type, String number_hours) {
        if(requestDialog == null || !requestDialog.isShowing()) {
            if (mediaPlayer == null || !mediaPlayer.isPlaying())
                mediaPlayer = MediaPlayer.create(this, R.raw.beep);
            requestDialog = new Dialog(this, R.style.DialogSlideAnim_leftright_Fullscreen);
            requestDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            requestDialog.setCancelable(false);
            requestDialog.setContentView(R.layout.dialog_request_layout);
            CircleImageView iv_location = requestDialog.findViewById(R.id.iv_location);
            if (map_img != null) {
                Log.e("asher", "map url static " + map_img);
                Glide.with(this).load(map_img).into(iv_location);
            }
            TextView tv_name = requestDialog.findViewById(R.id.tv_name);
            TextView tv_hours = requestDialog.findViewById(R.id.tv_hours);
            CircularProgressBar circularProgressBar = requestDialog.findViewById(R.id.req_progress_bar);
            tv_timer = requestDialog.findViewById(R.id.tv_timer);
            tv_name.setText(getResources().getString(R.string.txt_name) + " " + name);
            TextView tv_address = requestDialog.findViewById(R.id.tv_address);
            ImageButton btn_accept = requestDialog.findViewById(R.id.btn_accept);
            ImageButton btn_reject = requestDialog.findViewById(R.id.btn_reject);
            tv_address.setText(getResources().getString(R.string.txt_pic_address) + " " + address);
            if (req_type.equals("2")) {
                tv_hours.setVisibility(View.VISIBLE);
                tv_hours.setText(getResources().getString(R.string.txt_no_hours) + " " + number_hours);
            } else {
                tv_hours.setVisibility(View.GONE);
            }
            int animationDuration = (int) (countDown * 1000); // 2500ms = 2,5s
            circularProgressBar.setProgressWithAnimation(countDown * 2, animationDuration);
            countDownTimer = new MyCountDownTimer(countDown * 1000, interval);
            countDownTimer.start();
            btn_reject.setOnClickListener(v -> {
                mediaPlayer.stop();
                requestDialog.dismiss();
                driverRejected();
                countDownTimer.cancel();
                startCheckingUpcomingRequests();
            });
            btn_accept.setOnClickListener(v -> {
                mediaPlayer.stop();
                requestDialog.dismiss();
                requestAccepted();
                countDownTimer.cancel();

            });
            requestDialog.show();
        }
    }

    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopCheckingUpcomingRequests();
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
    }

    @OnClick(R.id.bnt_menu)
    public void onViewClicked() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            try {
                tv_timer.setText(getString(R.string.space) + (l / 1000));
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onFinish() {
            if (requestDialog != null && requestDialog.isShowing()) {
                requestDialog.cancel();
                mediaPlayer.stop();
                startCheckingUpcomingRequests();
            }
        }
    }

    private void openExitDialog() {
        final Dialog exit_dialog = new Dialog(this);
        exit_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exit_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        exit_dialog.setCancelable(true);
        exit_dialog.setContentView(R.layout.exit_layout);
        TextView tvExitOk = exit_dialog.findViewById(R.id.tvExitOk);
        TextView tvExitCancel = exit_dialog.findViewById(R.id.tvExitCancel);
        tvExitOk.setOnClickListener(view -> {
            exit_dialog.dismiss();
            finishAffinity();
        });
        tvExitCancel.setOnClickListener(view -> exit_dialog.dismiss());
        exit_dialog.show();
    }


    private void unregisterEventBus() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    private void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        requestStatusCheck();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

}
